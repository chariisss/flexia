import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import CImageCircle from '../CImageCircle';
import {colors, fonts} from '../../../utils';

const CCardTeacher = ({onPress}) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={onPress}>
        <View style={styles.profile}>
          <View style={styles.leftProfile}>
            <CImageCircle
              image={require('../../../assets/images/Teacher1.jpeg')}
            />
            <View style={styles.desc}>
              <Text style={styles.name}>Jesselyn Lawrance</Text>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <View style={styles.lesson}>
                  <Text style={styles.textLesson}>English</Text>
                </View>
              </View>
            </View>
          </View>
          <Text style={styles.price}>Rp. 120.000 / Hour</Text>
        </View>
        <Text style={styles.text}>
          Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem Lorem
          ipsum dolor sit amet Lorem ipsum dolor Lor...
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default CCardTeacher;

const styles = StyleSheet.create({
  container: {
    paddingVertical: 25,
    borderTopWidth: 0.2,
    borderColor: colors.gray,
  },
  profile: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 15,
  },
  text: {
    fontFamily: fonts.primary.regular,
    fontSize: 12,
    color: colors.gray,
  },
  leftProfile: {
    flexDirection: 'row',
  },
  name: {
    fontFamily: fonts.primary.regular,
    fontSize: 15,
  },
  desc: {
    marginLeft: 15,
  },
  lesson: {
    paddingVertical: 1,
    paddingHorizontal: 4,
    backgroundColor: colors.primary,
    borderRadius: 5,
    marginRight: 10,
  },
  textLesson: {
    fontFamily: fonts.primary.medium,
    fontSize: 12,
    color: colors.white,
  },
  price: {
    fontFamily: fonts.primary.regular,
    fontSize: 12,
    color: colors.gray,
  },
});
