import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import CGap from '../CGap';
import {
  IconTask,
  IconTime,
  IconUploadImage,
  IconUserBlue,
} from '../../../assets';
import {colors, fonts, responsiveHeight, responsiveWidth} from '../../../utils';

const CNotification = () => {
  return (
    <View style={styles.container}>
      <View style={styles.wrapper}>
        <IconTask />
        <Text style={styles.textBold}>New order booking</Text>
      </View>
      <CGap height={15} />
      <View>
        <View style={styles.wrapper}>
          <IconUserBlue />
          <View style={styles.wrapperText}>
            <Text style={styles.textRegular}>From</Text>
            <Text style={styles.textBold}>Delyne Northwell</Text>
          </View>
        </View>
        <View style={styles.wrapper}>
          <IconTime />
          <View style={styles.wrapperText}>
            <Text style={styles.textRegular}>Time</Text>
            <Text style={styles.textBold}>7 October 2021</Text>
          </View>
        </View>
        <View style={styles.wrapperImage}>
          <IconUploadImage />
        </View>
      </View>
    </View>
  );
};

export default CNotification;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 10,
    paddingVertical: 10,
    borderRightWidth: 0.3,
    borderBottomWidth: 0.3,
    borderLeftWidth: 0.3,
    borderColor: colors.gray,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 5,
  },
  textBold: {
    fontFamily: fonts.primary.semiBold,
    fontSize: 15,
    marginLeft: 20,
  },
  textRegular: {
    fontFamily: fonts.primary.regular,
    fontSize: 12,
    marginLeft: 20,
    color: colors.gray,
  },
  wrapperImage: {
    height: responsiveHeight(250),
    width: responsiveWidth(360),
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 0.2,
  },
});
