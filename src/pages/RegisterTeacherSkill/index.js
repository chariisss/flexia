import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  ScrollView,
  TextInput,
} from 'react-native';
import ModalSelector from 'react-native-modal-selector';
import {IconBack, IconUploadImage} from '../../assets';
import {CButton, CGap, CTextInput} from '../../components';
import {colors, fonts, responsiveHeight, responsiveWidth} from '../../utils';

export default class RegisterTeacherSkill extends Component {
  constructor(props) {
    super(props);

    this.state = {
      textInputValue: 'Select Category',
      levelSkill: 'Junior',
    };
  }

  render() {
    const {navigation} = this.props;
    const {textInputValue, levelSkill} = this.state;
    let index = 0;
    const data = [
      {key: index++, label: 'Programming'},
      {key: index++, label: 'Language'},
      {key: index++, label: 'Psychology'},
    ];
    const skill = [
      {key: index++, label: 'Junior'},
      {key: index++, label: 'Medium'},
      {key: index++, label: 'Senior'},
    ];
    return (
      <ScrollView style={styles.pages}>
        <TouchableOpacity
          style={{marginBottom: 30}}
          onPress={() => {
            navigation.goBack('');
          }}>
          <IconBack />
        </TouchableOpacity>
        <TouchableOpacity style={styles.wrapperUpload}>
          <IconUploadImage />
          <Text style={styles.textAddImage}>Add Image</Text>
        </TouchableOpacity>
        <CGap height={25} />
        <View style={styles.wrapperInput}>
          <Text style={styles.textTitle}>Specialization</Text>
          <CTextInput
            borderWidth={0.2}
            placeholder="Psychology, English Teacher, ect"
          />
        </View>
        <View style={styles.wrapperInput}>
          <Text style={styles.textTitle}>Price per hour</Text>
          <CTextInput borderWidth={0.2} placeholder="Rp. X00.000" />
        </View>
        <View style={styles.wrapperInput}>
          <Text style={styles.textTitle}>Pick Categories</Text>
          <ModalSelector
            data={data}
            initValue={textInputValue}
            onChange={op => {
              this.setState({textInputValue: op.label});
            }}
            overlayStyle={styles.input}
          />
        </View>
        <View style={styles.wrapperInput}>
          <Text style={styles.textTitle}>Input your skill</Text>
          <CTextInput borderWidth={0.2} placeholder="React, Psychology, ect" />
          <CGap height={5} />
          <ModalSelector
            data={skill}
            initValue={levelSkill}
            onChange={op => {
              this.setState({levelSkill: op.label});
            }}
          />
          <CGap height={15} />
          <CTextInput borderWidth={0.2} placeholder="React, Psychology, ect" />
          <CGap height={5} />
          <ModalSelector
            data={skill}
            initValue={levelSkill}
            onChange={op => {
              this.setState({levelSkill: op.label});
            }}
          />
          <CGap height={15} />
          <CTextInput borderWidth={0.2} placeholder="React, Psychology, ect" />
          <CGap height={5} />
          <ModalSelector
            data={skill}
            initValue={levelSkill}
            onChange={op => {
              this.setState({levelSkill: op.label});
            }}
          />
          <CGap height={15} />
        </View>
        <CTextInput
          textarea
          width={'100%'}
          height={responsiveHeight(500)}
          placeholder="Description"
        />
        <CGap height={25} />
        <CButton
          text="Register"
          onPress={() => {
            navigation.navigate('TeacherMainApp');
          }}
        />
        <CGap height={70} />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  pages: {
    flex: 1,
    backgroundColor: colors.whiteBlue,
    paddingHorizontal: 17,
    paddingTop: 30,
  },
  wrapperUpload: {
    height: responsiveHeight(250),
    width: '100%',
    borderWidth: 0.2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textAddImage: {
    fontFamily: fonts.primary.semiBold,
    fontSize: 17,
    marginVertical: 15,
  },
  textTitle: {
    fontFamily: fonts.primary.regular,
    fontSize: 15,
    marginBottom: 5,
  },
  wrapperInput: {
    marginBottom: 15,
  },
});
