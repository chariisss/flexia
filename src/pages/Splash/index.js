import React, {Component} from 'react';
import {StyleSheet, View, Image} from 'react-native';
import {colors, responsiveHeight, responsiveWidth} from '../../utils';

export default class Splash extends Component {
  componentDidMount() {
    setTimeout(() => {
      this.props.navigation.replace('Onboarding');
    }, 3000);
  }
  render() {
    return (
      <View style={styles.page}>
        <View style={styles.image}>
          <Image source={require('../../assets/images/Logo.png')} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  page: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.primary,
  },
  image: {
    width: responsiveWidth(238),
    height: responsiveHeight(210),
  },
});
