import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  ImageBackground,
  ScrollView,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  Keyboard,
  TouchableOpacity,
} from 'react-native';
import {colors, fonts, responsiveHeight, responsiveWidth} from '../../utils';
import {CGap, CTextInput} from '../../components';
import {CButton} from '../../components';

export default class Login extends Component {
  render() {
    const {navigation} = this.props;
    return (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
        style={styles.pages}>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.imageContainer}>
              <ImageBackground
                source={require('../../assets/images/LoginImage.png')}
                style={styles.image}></ImageBackground>
            </View>
            <View style={styles.bottom}>
              <Text style={styles.title}>Welcome Back !</Text>
              <Text style={styles.subTitle}>Login to your account</Text>
              <CGap height={5} />
              <CTextInput placeholder="Email" />
              <CGap height={10} />
              <CTextInput placeholder="Password" secureTextEntry />
              <CGap height={10} />
              <CButton
                text="Login"
                onPress={() => {
                  navigation.replace('MainApp');
                }}
              />
              <View style={styles.bottomText}>
                <Text style={styles.textLigth}>
                  Don't have an account?
                  <Text
                    style={styles.textMedium}
                    onPress={() => {
                      navigation.navigate('Register');
                    }}>
                    {' '}
                    Sign up
                  </Text>
                </Text>
                <Text style={styles.textLigth}>
                  Want to become a tutor?
                  <Text
                    style={styles.textMedium}
                    onPress={() => {
                      navigation.navigate('RegisterTeacher');
                    }}>
                    {' '}
                    Sign up as Tutor
                  </Text>
                </Text>
              </View>
            </View>
          </ScrollView>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  pages: {
    flex: 1,
  },
  imageContainer: {
    width: '100%',
    height: responsiveHeight(448),
  },
  image: {
    flex: 1,
    paddingHorizontal: 17,
    paddingTop: 10,
  },
  bottom: {
    width: '100%',
    height: responsiveHeight(471),
    zIndex: 1,
    marginTop: responsiveHeight(-60),
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
    backgroundColor: colors.whiteBlue,
    paddingTop: 15,
    paddingHorizontal: 17,
  },
  title: {
    fontSize: 20,
    fontFamily: fonts.primary.semiBold,
  },
  subTitle: {
    fontSize: 15,
    fontFamily: fonts.primary.regular,
    color: colors.gray,
  },
  bottomText: {
    alignItems: 'center',
    marginTop: 10,
  },
  textLigth: {
    fontSize: 14,
    fontFamily: fonts.primary.light,
    color: colors.gray,
    marginTop: 5,
  },
  textMedium: {
    color: colors.black,
    fontFamily: fonts.primary.medium,
  },
});
