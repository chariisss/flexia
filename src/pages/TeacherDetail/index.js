import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
} from 'react-native';
import {colors, fonts, responsiveHeight, responsiveWidth} from '../../utils';
import {IconBack} from '../../assets';
import {CImageCircle, CButton, CGap} from '../../components';

export default class TeacherDetail extends Component {
  render() {
    const {navigation} = this.props;
    return (
      <ScrollView style={styles.page} showsHorizontalScrollIndicator={false}>
        <TouchableOpacity
          style={{marginBottom: 30}}
          onPress={() => {
            navigation.goBack('');
          }}>
          <IconBack />
        </TouchableOpacity>
        <Image
          source={require('../../assets/images/Teacher1.jpeg')}
          style={styles.imageHeader}
        />
        <CGap height={15} />
        <View style={styles.photoName}>
          <CImageCircle image={require('../../assets/images/Teacher1.jpeg')} />
          <View style={{marginLeft: 15}}>
            <Text style={styles.name}>Jesselyn Lawrance</Text>
            <Text style={styles.location}>Jakarta, Indonesia</Text>
          </View>
        </View>
        <View style={styles.rating}>
          <View style={{alignItems: 'center'}}>
            <Text style={styles.name}>Rp. 120.000</Text>
            <Text style={styles.location}>per hour</Text>
          </View>
        </View>
        <View style={styles.border}>
          <CButton
            text="Message me"
            backgroundColor={colors.darkBlue}
            color={colors.lightBlue}
            onPress={() => {
              navigation.navigate('OnChat');
            }}
          />
        </View>
        <View style={styles.border}>
          <Text style={styles.name}>About Me</Text>
          <Text style={styles.biodata}>
            Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem Lorem
            ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor
            sit amet Lorem ipsum dolor sit amet Lorem Lorem ipsum dolor sit amet
            Lorem ipsum dolor sit amet
          </Text>
        </View>
        <View style={styles.border}>
          <Text style={styles.name}>My Skill</Text>
          <CGap height={10} />
          <View style={{flexDirection: 'row'}}>
            <View style={{paddingHorizontal: 10}}>
              <Text style={styles.skills}>English</Text>
            </View>
            <View style={styles.wrapperSkill}>
              <Text style={styles.textSkill}>Native</Text>
            </View>
          </View>
          <CGap height={10} />
          <View style={{flexDirection: 'row'}}>
            <View style={{paddingHorizontal: 10}}>
              <Text style={styles.skills}>Indonesia</Text>
            </View>
            <View style={styles.wrapperSkill}>
              <Text style={styles.textSkill}>Native</Text>
            </View>
          </View>
          <CGap height={10} />
          <View style={{flexDirection: 'row'}}>
            <View style={{paddingHorizontal: 10}}>
              <Text style={styles.skills}>German</Text>
            </View>
            <View style={styles.wrapperSkill}>
              <Text style={styles.textSkill}>Native</Text>
            </View>
          </View>
        </View>
        <CButton
          text="Book lesson"
          onPress={() => {
            navigation.navigate('PickDate');
          }}
        />
        <CGap height={70} />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: colors.whiteBlue,
    paddingHorizontal: 17,
    paddingTop: 30,
  },
  imageHeader: {
    width: responsiveWidth(380),
    height: responsiveHeight(246),
  },
  photoName: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 15,
  },
  name: {
    fontFamily: fonts.primary.medium,
    fontSize: 15,
  },
  location: {
    fontFamily: fonts.primary.regular,
    fontSize: 12,
    color: colors.gray,
  },
  rating: {
    paddingVertical: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderTopWidth: 0.2,
    borderColor: colors.gray,
  },
  textStar: {
    fontFamily: fonts.primary.medium,
    fontSize: 15,
    color: colors.orange,
    marginRight: 2,
  },
  star: {
    width: 13,
    height: 11.7,
    marginTop: -5,
    marginLeft: 3,
  },
  border: {
    paddingVertical: 20,
    borderTopWidth: 0.2,
    borderColor: colors.gray,
  },
  biodata: {
    fontFamily: fonts.primary.regular,
    fontSize: 12,
    color: colors.gray,
    marginVertical: 10,
  },
  wrapperSkill: {
    paddingHorizontal: 10,
    backgroundColor: colors.lightBlue,
    borderRadius: 10,
  },
  textSkill: {
    fontFamily: fonts.primary.regular,
    fontSize: 12,
    color: colors.primary,
  },
  textSkills: {
    fontFamily: fonts.primary.regular,
    fontSize: 12,
    color: colors.gray,
  },
});
