import React, {Component} from 'react';
import {Text, StyleSheet, View, ScrollView} from 'react-native';
import {CNotification} from '../../components';
import {colors, fonts} from '../../utils';

export default class NotifScreenTeacher extends Component {
  render() {
    return (
      <ScrollView style={styles.pages}>
        <Text style={styles.text}> Notification </Text>
        <CNotification />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  pages: {
    flex: 1,
    paddingHorizontal: 17,
    paddingTop: 30,
    backgroundColor: colors.whiteBlue,
  },
  text: {
    fontFamily: fonts.primary.semiBold,
    fontSize: 20,
    marginBottom: 20,
  },
});
