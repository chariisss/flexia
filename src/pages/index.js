import Splash from './Splash';
import Home from './Home';
import Chat from './Chat';
import Profile from './Profile';
import Login from './Login';
import OnboardingScreen from './Onboarding';
import Register from './Register';
import TeacherDetail from './TeacherDetail';
import OnChat from './OnChat';
import PickDate from './PickDate';
import Payment from './Payment';
import RegisterTeacher from './RegisterTeacher';
import RegisterTeacherSkill from './RegisterTeacherSkill';
import ChatScreenTeacher from './ChatScreenTeacher';
import ProfileScreenTeacher from './ProfileScreenTeacher';
import NotifScreenTeacher from './NotifScreenTeacher';
import EditProfile from './EditProfile';
import ChangePassword from './ChangePassword';
import TermCondition from './TermCondition';

export {
  Splash,
  Home,
  Chat,
  Profile,
  Login,
  OnboardingScreen,
  Register,
  TeacherDetail,
  OnChat,
  PickDate,
  Payment,
  RegisterTeacher,
  RegisterTeacherSkill,
  ChatScreenTeacher,
  ProfileScreenTeacher,
  NotifScreenTeacher,
  EditProfile,
  ChangePassword,
  TermCondition,
};
