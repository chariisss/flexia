import React, {Component} from 'react';
import {Text, StyleSheet, View, TouchableOpacity} from 'react-native';
import {IconBack, IconBCA, IconUploadImage} from '../../assets';
import {CButton, CGap} from '../../components';
import {colors, fonts, responsiveHeight} from '../../utils';

export default class Payment extends Component {
  render() {
    const {navigation} = this.props;
    return (
      <View style={styles.pages}>
        <View>
          <TouchableOpacity
            style={{marginBottom: 30}}
            onPress={() => {
              navigation.goBack('');
            }}>
            <IconBack />
          </TouchableOpacity>
          <Text style={styles.textMedium}>Please transfer to </Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}>
            <Text style={styles.textMedium}>Charisma Kurniawan Aji</Text>
            <IconBCA />
          </View>
          <View style={styles.wrapper}>
            <Text style={styles.textNumber}>497 108 7425</Text>
          </View>
          <Text style={styles.textMedium}>Amount Pay</Text>
          <View style={styles.wrapper}>
            <Text style={styles.textNumber}>Rp. 180.000</Text>
          </View>
        </View>
        <TouchableOpacity style={styles.wrapperUpload}>
          <IconUploadImage />
          <Text style={styles.textAddImage}>Add Image</Text>
        </TouchableOpacity>
        <CGap height={40} />
        <CButton
          text="I Have Complited Payment"
          fontFamily={fonts.primary.regular}
          onPress={() => {
            navigation.navigate('MainApp');
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  pages: {
    flex: 1,
    paddingHorizontal: 17,
    paddingTop: 30,
    backgroundColor: colors.whiteBlue,
  },
  textMedium: {
    fontFamily: fonts.primary.medium,
    fontSize: 15,
    marginBottom: 5,
  },
  textNumber: {
    fontFamily: fonts.primary.semiBold,
    fontSize: 15,
  },
  wrapper: {
    marginVertical: 10,
    paddingVertical: 10,
    paddingHorizontal: 20,
    backgroundColor: colors.lightBlue,
    borderRadius: 10,
  },
  wrapperUpload: {
    height: responsiveHeight(250),
    width: '100%',
    borderWidth: 0.2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textAddImage: {
    fontFamily: fonts.primary.semiBold,
    fontSize: 17,
    marginVertical: 15,
  },
});
