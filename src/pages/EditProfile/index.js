import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {IconBack} from '../../assets';
import {colors, fonts, responsiveWidth} from '../../utils';
import {CButton, CGap, CImageCircle, CTextInput} from '../../components';

export default class EditProfile extends Component {
  render() {
    const {navigation} = this.props;
    return (
      <View style={styles.pages}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack('');
            }}>
            <IconBack />
          </TouchableOpacity>
          <View style={styles.wrapperHeader}>
            <Text style={styles.title}>Edit Profile</Text>
            <CImageCircle
              backgroundColor={colors.primary}
              width={responsiveWidth(80)}
              height={responsiveWidth(80)}
            />
          </View>
          <Text style={styles.text}>Full name</Text>
          <CTextInput placeholder="Edit your full name" />
          <CGap height={15} />
          <Text style={styles.text}>Email</Text>
          <CTextInput placeholder="Edit your email" />
          <CGap height={15} />
          <Text style={styles.text}>Phone</Text>
          <CTextInput placeholder="Edit your phone number" />
        </ScrollView>
        <CGap height={100} />
        <TouchableOpacity style={styles.wrapperButton}>
          <CButton
            text="Save"
            onPress={() => {
              navigation.navigate('Profile');
            }}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  pages: {
    flex: 1,
    paddingHorizontal: 17,
    paddingTop: 30,
    backgroundColor: colors.whiteBlue,
  },
  title: {
    fontFamily: fonts.primary.medium,
    fontSize: 20,
    marginBottom: 20,
  },
  wrapperHeader: {
    alignItems: 'center',
    marginBottom: 20,
  },
  text: {
    fontFamily: fonts.primary.regular,
    fontSize: 12,
    color: colors.gray,
    marginBottom: 5,
  },
  wrapperButton: {
    position: 'absolute',
    bottom: 0,
    width: responsiveWidth(380),
    marginLeft: 17,
    marginBottom: 30,
  },
});
