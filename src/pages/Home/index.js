import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  ScrollView,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {IconArrowRight, IconSearch} from '../../assets';
import {colors, fonts, responsiveHeight, responsiveWidth} from '../../utils';
import {
  BannerSlider,
  CCardTeacher,
  CCircleCategories,
  CGap,
} from '../../components';

export default class Home extends Component {
  render() {
    const {navigation} = this.props;
    return (
      <ScrollView style={styles.page}>
        <View style={styles.header}>
          <IconSearch />
          <TextInput placeholder="Search keywords" style={styles.textInput} />
        </View>
        <CGap height={10} />
        <BannerSlider />
        <View style={styles.textCategoriesWrapper}>
          <Text style={styles.textCategories}>Categories</Text>
          <TouchableOpacity style={styles.arrowRight}>
            <IconArrowRight />
          </TouchableOpacity>
        </View>
        <View style={styles.circleCategories}>
          <CCircleCategories
            icon="Programming"
            backgroundColor={colors.lightBlue}
            categories="Programming"
          />
          <CCircleCategories
            icon="Language"
            backgroundColor={colors.lightYellow}
            categories="Language"
          />
          <CCircleCategories
            backgroundColor={colors.lightGreen}
            categories="Psychology"
          />
        </View>
        <CCardTeacher
          onPress={() => {
            navigation.navigate('TeacherDetail');
          }}
        />
        <CCardTeacher />
        <CGap height={70} />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  page: {
    flex: 1,
    paddingHorizontal: 17,
    backgroundColor: colors.whiteBlue,
  },
  header: {
    flex: 1,
    backgroundColor: colors.white,
    paddingLeft: 15,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 10,
    marginTop: 10,
  },
  textInput: {
    color: colors.black,
    fontFamily: fonts.primary.regular,
    width: '100%',
    marginLeft: 15,
    fontSize: 14,
  },
  textCategoriesWrapper: {
    flexDirection: 'row',
    marginVertical: 10,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  textCategories: {
    fontFamily: fonts.primary.semiBold,
    fontSize: 17,
    color: colors.black,
  },
  arrowRight: {
    width: responsiveWidth(30),
    height: responsiveHeight(26),
    alignItems: 'center',
    justifyContent: 'center',
  },
  circleCategories: {
    flexDirection: 'row',
    marginBottom: 15,
  },
});
