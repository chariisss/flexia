import React, {Component} from 'react';
import {Text, StyleSheet, View, Image, TouchableOpacity} from 'react-native';
import {colors, fonts, responsiveWidth} from '../../utils';
import {IconArrowRight, IconUser} from '../../assets';
import {dummyMenu} from '../../data/DummyMenu';
import {CCardMenu} from '../../components';

export default class ProfileScreenTeacher extends Component {
  constructor(props) {
    super(props);

    this.state = {
      menu: dummyMenu,
    };
  }

  render() {
    const {menu} = this.state;
    const {navigation} = this.props;
    return (
      <View style={styles.pages}>
        <View style={styles.wrapperImage}>
          <View style={styles.circle}>
            <Image
              source={require('../../assets/images/Teacher0.jpeg')}
              style={styles.image}
            />
          </View>
          <View>
            <Text style={styles.name}>Charisma Kurniawan</Text>
            <Text style={styles.location}>Jakarta, Indonesia</Text>
          </View>
        </View>
        <Text style={styles.account}>Account Setting</Text>
        {menu.map(menu => {
          return (
            <CCardMenu
              menu={menu}
              key={menu.id}
              navigation={this.props.navigation}
            />
          );
        })}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  pages: {
    flex: 1,
    backgroundColor: colors.whiteBlue,
    paddingHorizontal: 17,
    paddingTop: 30,
  },
  wrapperImage: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  circle: {
    width: responsiveWidth(70),
    height: responsiveWidth(70),
    backgroundColor: 'black',
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden',
    marginRight: 15,
  },
  image: {
    width: responsiveWidth(70),
    height: responsiveWidth(70),
  },
  name: {
    fontFamily: fonts.primary.semiBold,
    fontSize: 20,
  },
  location: {
    fontFamily: fonts.primary.regular,
    fontSize: 15,
    color: colors.gray,
  },
  account: {
    fontFamily: fonts.primary.regular,
    fontSize: 15,
    marginVertical: 20,
  },
});
