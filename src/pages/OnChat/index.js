import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {IconBack} from '../../assets';
import {CGap, CImageCircle, CTextInput} from '../../components';
import {colors, fonts, responsiveWidth} from '../../utils';

export default class OnChat extends Component {
  render() {
    const {navigation} = this.props;
    return (
      <View style={styles.page}>
        <View style={styles.topBar}>
          <TouchableOpacity
            style={{marginRight: 40}}
            onPress={() => {
              navigation.goBack('');
            }}>
            <IconBack />
          </TouchableOpacity>
          <CImageCircle
            image={require('../../assets/images/Teacher1.jpeg')}
            height={responsiveWidth(40)}
            width={responsiveWidth(40)}
          />
          <Text style={styles.name}>Jesselyn Lawrance</Text>
        </View>
        <ScrollView>
          <View style={styles.leftChat}>
            <Text>
              Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem Lorem
              ipsum dolor sit amet Lorem ipsum dolor Lor...
            </Text>
          </View>
          <View style={styles.rightChat}>
            <Text>
              Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem Lorem
              ipsum dolor sit amet Lorem ipsum dolor Lor...
            </Text>
          </View>
        </ScrollView>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <CTextInput width={responsiveWidth(320)} borderWidth={0.2} />
          <TouchableOpacity>
            <CImageCircle
              image={require('../../assets/images/ArrowTop.png')}
              width={responsiveWidth(40)}
              height={responsiveWidth(40)}
              backgroundColor={colors.primary}
            />
          </TouchableOpacity>
        </View>
        <CGap height={15} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  page: {
    flex: 1,
    paddingHorizontal: 17,
    paddingTop: 30,
  },
  topBar: {
    flexDirection: 'row',
    marginBottom: 15,
    alignItems: 'center',
  },
  name: {
    marginLeft: 15,
    fontFamily: fonts.primary.medium,
    fontSize: 17,
  },
  leftChat: {
    maxWidth: responsiveWidth(280),
    padding: 10,
    backgroundColor: colors.lightYellow,
    borderRadius: 10,
    marginBottom: 40,
    left: 0,
  },
  rightChat: {
    maxWidth: responsiveWidth(280),
    padding: 10,
    backgroundColor: colors.lightBlue,
    borderRadius: 10,
    right: 0,
    marginLeft: responsiveWidth(100),
  },
});
