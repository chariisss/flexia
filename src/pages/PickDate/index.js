import React, {Component} from 'react';
import {Text, StyleSheet, View, TouchableOpacity, Button} from 'react-native';
import {IconBack} from '../../assets';
import {CButton, CGap, CImageCircle} from '../../components';
import {colors, fonts, responsiveWidth} from '../../utils';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import {convertDate, convertTimestamp} from '../../utils/util/date';

export default class PickDate extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isDatePickerVisible: false,
      date: '',
    };
  }

  _showDatePicker = () => {
    this.setState({
      isDatePickerVisible: true,
    });
  };

  _hideDatePicker = () => {
    this.setState({
      isDatePickerVisible: false,
    });
  };

  _handleConfirm = date => {
    if (convertTimestamp(date) <= convertTimestamp(new Date())) {
      alert('Cannot pick date');
    } else {
      this.setState({date: convertDate(date)});
    }
    this._hideDatePicker();
  };

  render() {
    const {navigation} = this.props;
    const {isDatePickerVisible, date} = this.state;
    return (
      <View style={styles.pages}>
        <TouchableOpacity
          style={{marginBottom: 30}}
          onPress={() => {
            navigation.goBack('');
          }}>
          <IconBack />
        </TouchableOpacity>
        <View style={styles.topBar}>
          <View>
            <Text style={styles.textTitle}>1 day lesson</Text>
            <Text style={styles.textSub}>Select a day below</Text>
          </View>
          <CImageCircle image={require('../../assets/images/Teacher1.jpeg')} />
        </View>
        <CGap height={20} />
        <CButton
          backgroundColor={colors.darkBlue}
          text="Pick Date"
          onPress={this._showDatePicker}
        />
        <DateTimePickerModal
          isVisible={isDatePickerVisible}
          mode="date"
          onConfirm={this._handleConfirm}
          onCancel={this._hideDatePicker}
        />
        <CGap height={25} />
        <Text style={styles.date}>Date selected :</Text>
        <Text style={styles.date}>{date}</Text>
        <CGap height={25} />
        <View style={styles.wrapperButton}>
          <CButton
            text="Continue"
            onPress={() => {
              navigation.navigate('Payment');
            }}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  pages: {
    flex: 1,
    paddingHorizontal: 17,
    paddingTop: 30,
    backgroundColor: colors.whiteBlue,
  },
  topBar: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
    paddingBottom: 25,
    borderBottomWidth: 0.2,
    borderColor: colors.gray,
  },
  textTitle: {
    fontFamily: fonts.primary.semiBold,
    fontSize: 20,
  },
  textSub: {
    fontFamily: fonts.primary.regular,
    fontSize: 15,
    color: colors.gray,
  },
  date: {
    fontFamily: fonts.primary.medium,
    fontSize: 15,
  },
  wrapperButton: {
    position: 'absolute',
    bottom: 0,
    width: responsiveWidth(380),
    marginLeft: 17,
    marginBottom: 30,
  },
});
