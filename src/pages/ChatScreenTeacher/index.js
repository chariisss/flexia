import React, {Component} from 'react';
import {Text, StyleSheet, View} from 'react-native';
import {CCardChat} from '../../components';
import {colors, fonts} from '../../utils';

export default class ChatScreenTeacher extends Component {
  render() {
    const {navigation} = this.props;
    return (
      <View style={styles.pages}>
        <Text style={styles.text}>Messages</Text>
        <CCardChat
          onPress={() => {
            navigation.navigate('OnChat');
          }}
        />
        <CCardChat />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  pages: {
    flex: 1,
    paddingHorizontal: 17,
    paddingTop: 30,
    backgroundColor: colors.whiteBlue,
  },
  text: {
    fontFamily: fonts.primary.semiBold,
    fontSize: 20,
    marginBottom: 20,
  },
});
